"""
Chutes & Ladders Game

- v1: Board implemented as class
- v2: Game implemented as class
- v3: Experiment implemented as class
- v4: Multiple players and configurable board
- v5: Board subclasses
- v6: Refactor Board classes to avoid code duplication
- v7: Support loading board specifications from YML file

After class 24 October 2022.
"""

import random
import matplotlib.pyplot as plt
import numpy as np
import yaml


class Board:

    GOAL_TYPE = 'passing'

    def __init__(self, goal=25, chutes_and_ladders=None, board_file=None):
        if board_file is not None:
            specs = yaml.safe_load(open(board_file))
            self.goal = specs['goal']
            self.chutes_and_ladders = {**specs['chutes'], **specs['ladders']}
        else:
            self.goal = goal
            if chutes_and_ladders is None:
                self.chutes_and_ladders = {1: 12, 13: 22, 14: 3, 20: 8}
            else:
                self.chutes_and_ladders = chutes_and_ladders

    def goal_reached(self, position):
        return position >= self.goal

    def new_position(self, position, step):
        new_pos = position + step

        new_pos = self.check_against_goal(new_pos, position)

        if new_pos in self.chutes_and_ladders:
            return self.chutes_and_ladders[new_pos]
        else:
            return new_pos

    def check_against_goal(self, new_pos, position):
        return new_pos

    def description(self):
        # For every start-end pair representing a chute, the comparison start > end
        # is True, for each ladder pair it is False. When summing over True/False values,
        # True counts as 1 and False as 0, so we obtain the number of chutes.
        n_chutes = sum(start > end for start, end in self.chutes_and_ladders.items())
        n_ladders = len(self.chutes_and_ladders) - n_chutes

        return f"{n_chutes} chutes, {n_ladders} ladders, goal {self.goal} ({self.GOAL_TYPE})"


class BlockingBoard(Board):

    GOAL_TYPE = 'blocking'

    def check_against_goal(self, new_pos, position):
        if new_pos > self.goal:
            new_pos = position
        return new_pos


class ReflectingBoard(Board):

    GOAL_TYPE = 'reflecting'

    def check_against_goal(self, new_pos, position):
        if new_pos > self.goal:
            overstepped = new_pos - self.goal
            new_pos = self.goal - overstepped
        return new_pos


class Player:
    def __init__(self, board):
        self.board = board
        self.position = 0
        self.num_moves = 0

    def make_move(self):
        step = random.randint(1, 6)
        self.position = self.board.new_position(self.position, step)
        self.num_moves += 1

    def goal_reached(self):
        return self.board.goal_reached(self.position)


class Game:
    def __init__(self, board, num_players):
        self.board = board
        self.num_players = num_players

    def play(self):
        players = [Player(self.board) for _ in range(self.num_players)]
        while not any(player.goal_reached() for player in players):
            for player in players:
                player.make_move()

        return player.num_moves


class Experiment:
    def __init__(self, num_games, seed, board, num_players):
        random.seed(seed)
        self.board = board
        self.num_games = num_games
        self.num_players = num_players

    def execute(self):
        return [Game(self.board, self.num_players).play() for _ in range(self.num_games)]


if __name__ == "__main__":

    plt.figure(figsize=(8, 3))

    for board in [Board(), ReflectingBoard(), BlockingBoard(),
                  Board(board_file='stigespill_board.yml')]:

        exper = Experiment(1000, 1710, board, 1)
        durations = exper.execute()

        print(f'Board type            : {board.description()}')
        print(f'Shortest game duration: {min(durations):4d}')
        print(f'Mean game duration    : {np.mean(durations):6.1f} ± {np.std(durations):.1f}')
        print(f'Longest game duration : {max(durations):4d}')

        hv, hb = np.histogram(durations, bins=np.arange(0, max(durations)))
        plt.step(hb[:-1], hv, label=board.description())
        plt.legend()

    plt.show()
