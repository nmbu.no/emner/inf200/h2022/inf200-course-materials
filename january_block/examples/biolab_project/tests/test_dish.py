"""
Tests for the Dish class.
"""
__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

import pytest
import itertools
from scipy.stats import binom_test

from biolab.dish import Dish
from biolab.bacteria import Bacteria

# acceptance limit for statistical tests
ALPHA = 0.001


def test_dish_create():
    """Test that dish is created with correct number of bacteria."""

    n_a, n_b = 23, 41
    d = Dish(n_a, n_b)
    assert d.get_num_a() == n_a and d.get_num_b() == n_b


def test_dish_aging_calls(mocker):
    """
    Test that dish aging method calls bacteria aging method the right number of times.
    """

    # mocker.spy wraps Bacteria.ages() so that we can get
    # a call count (and more information if we wanted)
    mocker.spy(Bacteria, 'ages')

    n_a, n_b = 10, 20
    d = Dish(n_a, n_b)
    d.aging()

    # noinspection PyUnresolvedReferences
    assert Bacteria.ages.call_count == n_a + n_b


def test_dish_aging():
    """
    Test that bacteria age correctly via Dish.aging().
    """

    n_a, n_b = 5, 7
    d = Dish(n_a, n_b)
    d.aging()

    # all bacteria had age 0 initially, must now be 1
    assert [b.age for b in itertools.chain(d.a_pop, d.b_pop)] == [1] * (n_a + n_b)


# The following parameterization ensures that all tests in
# the class run with (n_a, n_b)==(10, 20) and ==(30, 40)
@pytest.mark.parametrize('n_a, n_b', [(10, 20), (30, 40)])
class TestDeathDivision:
    """
    Tests for death and division.

    The main point of this test class is to show the use of a fixture
    to create an initial population before each test.
    """

    @pytest.fixture(autouse=True)
    def create_dish(self, n_a, n_b):
        """Create dish with bacteria numbers supplied by fixture."""
        self.n_a = n_a
        self.n_b = n_b
        self.dish = Dish(self.n_a, self.n_b)

    @pytest.fixture
    def reset_bacteria_defaults(self):
        # no setup
        yield

        # reset class parameters to default values after each test
        Bacteria.set_params(Bacteria.default_params)

    def test_death(self):
        n_a_old = self.dish.get_num_a()
        n_b_old = self.dish.get_num_b()

        for _ in range(10):
            self.dish.death()
            n_a = self.dish.get_num_a()
            n_b = self.dish.get_num_b()
            # n_a and n_b must never increase
            assert n_a <= n_a_old
            assert n_b <= n_b_old
            n_a_old, n_b_old = n_a, n_b

        # after 10 rounds of death probability of no change is minimal
        assert self.dish.get_num_a() < self.n_a and self.dish.get_num_b() < self.n_b

    def test_division(self):
        n_a_old = self.dish.get_num_a()
        n_b_old = self.dish.get_num_b()

        for _ in range(10):
            self.dish.division()
            n_a = self.dish.get_num_a()
            n_b = self.dish.get_num_b()
            # n_a and n_b must never decrease
            assert n_a >= n_a_old
            assert n_b >= n_b_old
            n_a_old, n_b_old = n_a, n_b

        # after 10 rounds of death probability of no change is minimal
        assert self.dish.get_num_a() > self.n_a and self.dish.get_num_b() > self.n_b

    def test_all_die(self, reset_bacteria_defaults):
        Bacteria.set_params({'p_death': 1.0})
        self.dish.death()
        assert self.dish.get_num_a() == 0 and self.dish.get_num_b() == 0

    # Each value for p_death will be combined with each value
    # of (n_a, n_b)
    @pytest.mark.parametrize('p_death', [0.1, 0.9, 0.5])
    def test_death_multi_p(self, reset_bacteria_defaults, p_death):

        Bacteria.set_params({'p_death': p_death})
        n_a0 = self.dish.get_num_a()
        n_b0 = self.dish.get_num_b()
        self.dish.death()
        died_a = n_a0 - self.dish.get_num_a()
        died_b = n_b0 - self.dish.get_num_b()

        pass_a = binom_test(died_a, n_a0, p_death) > ALPHA
        pass_b = binom_test(died_b, n_b0, p_death) > ALPHA

        assert pass_a and pass_b
