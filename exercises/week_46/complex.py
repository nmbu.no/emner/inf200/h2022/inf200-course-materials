"""
Example solution for Deliverable 3 with challenge.

This module provides a complex number class.
"""

__author__ = "Hans Ekkehard Plesser / NMBU"


import math


class Complex:
    """
    Complex number class supporting cartesian and polar coordinates
    and all arithmetic operations.
    """

    # Default None for all so we can check that either re/im or r/phi are given.
    # * in argument list means r and phi must be given as named parameters
    def __init__(self, re=None, im=None, *, r=None, phi=None):
        """
        Complex numbers can either be initialised with cartesian or
        polar coordinates, but not a mixture of both.

        :param re: real part
        :param im: imaginary part
        :param r: absolute value (modulus, magnitude)
        :param phi: argument (angle, in radian)

        Examples:
        Complex()     -> 0 + 0i
        Complex(1)    -> 1 + 0i
        Complex(im=1) -> 0 + 1i
        Complex(1, 2) -> 1 + 2i
        Complex(r=1)  -> 1 + 0i
        Complex(r=1, phi=2) -> 1 * exp(2i)
        """

        cart = re is not None or im is not None
        polar = r is not None or phi is not None

        if cart and polar:
            raise ValueError('Cannot mix cartesian and polar coordinates.')
        if polar and r is None:
            raise ValueError('Polar coordinates require absolute value r.')

        if cart:
            self._re = re if re is not None else 0
            self._im = im if im is not None else 0
            self._r = None
            self._phi = None
        elif polar:
            self._r = r
            self._phi = phi if phi is not None else 0
            self._re = None
            self._im = None
        else:  # no arguments given
            self._re = 0
            self._im = 0
            self._r = None
            self._phi = None

    @property
    def re(self):
        if self._re is None:
            self._re = self._r * math.cos(self._phi)
        return self._re

    @property
    def im(self):
        if self._im is None:
            self._im = self._r * math.sin(self._phi)
        return self._im

    @property
    def r(self):
        if self._r is None:
            self._r = math.sqrt(self._re**2 + self._im**2)
        return self._r

    @property
    def phi(self):
        if self._phi is None:
            self._phi = math.atan2(self._im, self._re)
        return self._phi

    @property
    def conj(self):
        return Complex(self.re, -self.im)

    def __repr__(self):
        return f"Complex({self.re}, {self.im})"

    def __str__(self):
        return f"{self.re}{self.im:+}i"

    def __eq__(self, rhs):
        # Allow float or int as rhs
        try:
            return self.re == rhs.re and self.im == rhs.im
        except AttributeError:
            return self.re == rhs and self.im == 0
    
    def __ne__(self, rhs):
        return not self == rhs
    
    def __neg__(self):
        return Complex(-self.re, -self.im)

    def __add__(self, rhs):
        # allow adding non-complex numbers
        try:
            return Complex(self.re + rhs.re, self.im + rhs.im)
        except AttributeError:
            return Complex(self.re + rhs, self.im)

    def __radd__(self, lhs):
        return self + lhs

    def __sub__(self, rhs):
        # map to __neg__ and __add__
        return self + (-rhs)

    def __rsub__(self, lhs):
        return Complex(lhs) - self

    def __mul__(self, rhs):
        # allow multiplication by non-complex number
        try:
            return Complex(self.re*rhs.re - self.im*rhs.im,
                           self.re*rhs.im + self.im*rhs.re)
        except AttributeError:
            return Complex(self.re * rhs, self.im * rhs)

    def __rmul__(self, lhs):
        return self * lhs

    def __truediv__(self, rhs):
        # Division of complex by complex can only easily be expressed in polar coordinates
        try:
            return Complex(r=self.r/rhs.r, phi=self.phi-rhs.phi)
        except AttributeError:
            return Complex(r=self.r / rhs, phi=self.phi)

    def __rtruediv__(self, lhs):
        # Division of real number by complex
        return lhs * self**(-1)

    def __pow__(self, rhs):
        try:
            # Exponentiation requires polar and cartesian coordinates
            r, phi = self.r, self.phi
            c, d = rhs.re, rhs.im
            if r == 0:
                if rhs.r == 0:
                    return Complex(1)   # 0^0 == 1 by definition
                elif c > 0:
                    return Complex(0)   # 0^(c + di) == 0 if c > 0
                else:
                    raise ValueError('Undefined value.')
            return Complex(r=r**c * math.exp(-phi * d),
                           phi=c*phi + d*math.log(r))
        except AttributeError:
            # Exponent is scalar. Convert to Complex rhs + 0i and try again.
            return self**Complex(rhs)

    def __rpow__(self, lhs):
        # Raise scalar to complex power
        return Complex(lhs)**self
