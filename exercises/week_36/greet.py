"""
Generate a simple greeting.

Ask user to input a name and print a greeting including that name.

Hans Ekkehard Plesser / NMBU
"""

name = input("Please enter your name: ")
print()
print(f"Welcome to INF200, {name}!")
