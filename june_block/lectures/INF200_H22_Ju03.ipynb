{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# INF200 Lecture No Ju03\n",
    "### Hans Ekkehard Plesser / NMBU\n",
    "### 07 June 2023\n",
    "\n",
    "## Today's topics\n",
    "\n",
    "- Keeping your code tidy\n",
    "- More on Python \n",
    "    - Deleting from lists\n",
    "    - `isinstance()` considered harmful\n",
    "    - Creating new subclass instances elegantly\n",
    "    - A little more on randomness\n",
    "- More on testing\n",
    "    - Levels of testing\n",
    "    - Test file names\n",
    "    - Placement of test files\n",
    "    - Suggestions for test design\n",
    "    - Approximate comparisons\n",
    "    - Test parameterization\n",
    "    - Test classes with setup and teardown features\n",
    "    - Mocking\n",
    "    - Tests involving randomness"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "-------\n",
    "\n",
    "# Keeping your code tidy\n",
    "\n",
    "- Run `Code > Inspect code` regularly on your code\n",
    "- Fix weaknesses reported\n",
    "- Also keep an eye on typos\n",
    "- Formatting checks are run automatically on GitLab (details soon)\n",
    "- To locally run exactly the same tests that are run on GitLab\n",
    "    1. open a `Terminal` in PyCharm\n",
    "    1. run `flake8 src tests`\n",
    "- In-class example: `examples/biolab_project`\n",
    "\n",
    "## Getting the right `flake8` version\n",
    "\n",
    "- The tests applied by `flake8` can change with `flake8` versions.\n",
    "- To get consistent results, it can be useful to fix the `flake8` version.\n",
    "- We will use the currently newest version, 6.0.\n",
    "\n",
    "### Checking the flake8 version\n",
    "\n",
    "In a terminal with the conda environment you use for BioSim activated, run\n",
    "```\n",
    "flake8 --version\n",
    "```\n",
    "This will output a line like\n",
    "```\n",
    "6.0.0 (mccabe: 0.7.0, pycodestyle: 2.10.0, pyflakes: 3.0.1) CPython 3.10.8 on Darwin\n",
    "```\n",
    "If the line starts with `6.0.x`, you are all set.\n",
    "\n",
    "### Fix: update flake8 with pip\n",
    "\n",
    "If you have a different `flake8` version than some 6.0 version, run with the conda environment active\n",
    "```\n",
    "pip install \"flake8==6.0\"\n",
    "```\n",
    "Note the `\"\"`, they are important, and it must be `==`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "---------------\n",
    "# More on Python\n",
    "\n",
    "## Deleting from lists\n",
    "\n",
    "- Removing elements from a list inside a loop over the list is dangerous\n",
    "- It can confuse the list iteration\n",
    "- Example: remove numbers that can be divided by 2 or 3\n",
    "\n",
    "### A correct loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Testing 0 ... divisible\n",
      "Testing 1\n",
      "Testing 2 ... divisible\n",
      "Testing 3 ... divisible\n",
      "Testing 4 ... divisible\n",
      "Testing 5\n",
      "Testing 6 ... divisible\n",
      "Testing 7\n",
      "Testing 8 ... divisible\n",
      "Testing 9 ... divisible\n"
     ]
    }
   ],
   "source": [
    "d = list(range(10))\n",
    "for n in d:\n",
    "    print('Testing', n, end='')\n",
    "    if n % 2 == 0 or n % 3 == 0:\n",
    "        print(' ... divisible', end='')\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A confused loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Testing 0\n",
      "Testing 2\n",
      "Testing 4\n",
      "Testing 6\n",
      "Testing 8\n",
      "[1, 3, 5, 7, 9]\n"
     ]
    }
   ],
   "source": [
    "d = list(range(10))\n",
    "for n in d:\n",
    "    print('Testing', n)\n",
    "    if n % 2 == 0 or n % 3 == 0:\n",
    "        d.remove(n)\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A better solution: keep the good ones"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 5, 7]\n"
     ]
    }
   ],
   "source": [
    "d = list(range(10))\n",
    "d = [n for n in d if not (n % 2 == 0 or n % 3 == 0)]\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `isinstance()` considered harmful\n",
    "\n",
    "- Do not use \n",
    "    - `if isinstance() ...` \n",
    "    - `if type() == ...`\n",
    "    - `if cell.code == 'L' ...`\n",
    "\n",
    "- If you are tempted to do so, in 99.9% of cases you are trying to hack a solution that could be achieved much more elegantly and robustly using proper object oriented design.\n",
    "- An object shall know itself how to \"behave\" (through proper member functions).\n",
    "- For historical reference on the headline, see https://en.wikipedia.org/wiki/Considered_harmful."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating new subclass instances elegantly (aka \"birth\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A class allowing instances to create new instances of the class\n",
    "\n",
    "New objects are created\n",
    "- with a given probability $p$\n",
    "- may fail to be created if the chosen \"weight\" is too large\n",
    "- `clone()` returns new object or `None`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Q:\n",
    "    \n",
    "    p = 0.5\n",
    "    max_w = 5\n",
    "    \n",
    "    def __init__(self, w):\n",
    "        assert w > 0\n",
    "        self.w = w \n",
    "        \n",
    "    def __repr__(self):\n",
    "        return f'Q({self.w:.2g})'\n",
    "        \n",
    "    def clone(self):\n",
    "        if random.random() < self.p:\n",
    "            nw = random.lognormvariate(0, 1)\n",
    "            if nw <= self.max_w:\n",
    "                return Q(nw)\n",
    "        return None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[None, None, Q(1.4), Q(0.43), Q(3.1), None, Q(0.83), None, None, None]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "random.seed(123456)\n",
    "q = Q(10)\n",
    "[q.clone() for _ in range(10)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A class hierarchy with similar properties\n",
    "\n",
    "- `A` is an abstract base class \n",
    "- Only objects of subclasses `B` and `C` can be instantiated\n",
    "- Cloning is still done in the base class using `type()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "class A:\n",
    "    \n",
    "    p = None\n",
    "    max_w = None\n",
    "    \n",
    "    def __init__(self, w):\n",
    "        assert w > 0\n",
    "        self.w = w \n",
    "        \n",
    "    def __repr__(self):\n",
    "        return f'{type(self).__name__}({self.w:.2g})'\n",
    "        \n",
    "    def clone(self):\n",
    "        if random.random() < self.p:\n",
    "            nw = random.lognormvariate(0, 1)\n",
    "            if nw <= self.max_w:\n",
    "                return type(self)(nw)\n",
    "        return None        \n",
    "\n",
    "class B(A):\n",
    "    p = 0.5\n",
    "    max_w = 5\n",
    "\n",
    "class C(A):\n",
    "    p = 0.3\n",
    "    max_w = 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[B(0.58), B(1.1), B(0.77), B(1.2), None, B(4), None, B(1.6), B(0.29), None]"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "random.seed(12345)\n",
    "b = B(10)\n",
    "[b.clone() for _ in range(10)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[None, C(2.2), None, C(1.1), C(0.77), C(1.2), None, None, None, C(1.6)]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "random.seed(12345)\n",
    "c = C(10)\n",
    "[c.clone() for _ in range(10)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A function to produce many clones\n",
    "\n",
    "- Takes a list of objects\n",
    "- Gives every object the opportunity to clone\n",
    "- Returns list of only those objects that were cloned (drops `None`s)\n",
    "\n",
    "#### First implementation: explicit loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mc1(d):\n",
    "    r = []\n",
    "    for x in d:\n",
    "        xc = x.clone()\n",
    "        if xc:\n",
    "            r.append(xc)\n",
    "    return r"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Second implementation: list comprehension\n",
    "\n",
    "- Use *assignment expression*\n",
    "- Works only in Python 3.8 and later"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mc2(d):\n",
    "    return [xc for x in d if (xc := x.clone())]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Test both implementations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[B(10), C(20), B(10), B(0.58), C(1.1), B(0.77)]\n",
      "[B(10), C(20), B(10), B(0.58), C(1.1), B(0.77)]\n"
     ]
    }
   ],
   "source": [
    "for f in [mc1, mc2]:\n",
    "    random.seed(12345)\n",
    "    d = [B(10), C(20), B(10)]\n",
    "    new_d = f(d)\n",
    "    d.extend(new_d)\n",
    "    print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## A little more on randomness\n",
    "\n",
    "### Obtaining uniform random numbers on $[0, 1)$\n",
    "\n",
    "- Use `random.random()` for this, even though [documentation](https://docs.python.org/3/library/random.html#real-valued-distributions) is not entirely clear (but see [here](https://docs.python.org/3/library/random.html#))\n",
    "\n",
    "### Drawing lognormally distributed weights\n",
    "- Use `random.lognormvariate(mu, sigma)` to draw birth weights with\n",
    "$$\n",
    "\\mu = \\ln\\left(\\frac{\\mu_X^2}{\\sqrt{\\mu_X^2+\\sigma_X^2}}\\right)\n",
    "\\qquad\\text{and}\\qquad\n",
    "\\sigma = \\sqrt{\\ln\\left(1+\\frac{\\sigma_X^2}{\\mu_X^2}\\right)}\n",
    "$$\n",
    "where $\\mu_X$=`w_birth`and $\\sigma_X$=`sigma_birth` ([equations after Wikipedia](https://en.wikipedia.org/wiki/Log-normal_distribution#Generation_and_parameters)).\n",
    "\n",
    "### Choosing between two alternatives\n",
    "\n",
    "- An animal has a probability $p$ to die\n",
    "- How do we decided if the animal will die in a given year?\n",
    "    - Draw uniformly distributed random number from $[0, 1)$ and compare to $p$\n",
    "    \n",
    "### Choosing between multiple alternaives\n",
    "\n",
    "- Literature: Knuth, The Art of Computer Programming, vol 2, ch 3.3-3.4 \n",
    "- In a simluation, we want to choose between four alternatives with probabilities $p_0, p_1, p_2, p_3$\n",
    "- Note $\\sum_{n=0}^3 p_n = 1$ by definition\n",
    "- Cumulative probabilities $P_n = \\sum_{k=0}^n p_k$ divide unit interval in sections corresponding to events 0, 1, 2, 3\n",
    "- Specifically, we choose a random number $r$ and select \n",
    "\n",
    "\\begin{equation}\n",
    "\\begin{cases}\n",
    "\\text{event}\\: 0 \\quad\\text{if}\\; r < P_0 \\\\\n",
    "\\text{event}\\: n \\quad\\text{if}\\; P_{n-1} \\leq r < P_{n}\\;\\; \\text{for}\\; n>0\n",
    "\\end{cases}\n",
    "\\end{equation}\n",
    "\n",
    "- The following code will select from `len(p)` alternatives with probabilities `p[0]`, `p[1]`, ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "def random_select(p):\n",
    "    r = random.random()\n",
    "    n = 0\n",
    "    while r >= p[n]:\n",
    "        r -= p[n]\n",
    "        n += 1\n",
    "    return n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simpler approach for our simulations\n",
    "\n",
    "- Animals move in all four directions with *same* probability\n",
    "- Can use `random.choice()` to pick one element from a list with equal probability"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "------------------\n",
    "\n",
    "# More on testing\n",
    "\n",
    "## Levels of testing\n",
    "\n",
    "- *unit tests* are tests of small parts of code\n",
    "    - test individual methods\n",
    "- *integration tests* test that the parts of a larger project work together\n",
    "    - test that class instances behave as expected\n",
    "    - expect that a class, e.g., representing a landscape cell, properly manages animals\n",
    "- *acceptance tests* test that the software as a whole\n",
    "    - `check_sim.py`\n",
    "    - `test_biosim_interface.py`\n",
    "    - similar simulations, e.g., with parameter modifications\n",
    "        - different islands and initial populations\n",
    "        - parameter choices preventing birth, death, eating, movement, ...\n",
    "- *regression tests* are added when a bug is discovered\n",
    "    - the test reproduces the bug\n",
    "    - when the bug is fixed, the test passes\n",
    "    - we keep the test, in case we should re-introduce the bug by a later change (regression)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "\n",
    "## Test file names\n",
    "\n",
    "- You should write different test modules (files) to keep everything neat and organized\n",
    "- Rule of thumb: One test module for each module in your package\n",
    "    - `animals.py` ---> `test_animals.py`\n",
    "    - `landscape.py` ---> `test_landscape.py`\n",
    "    - ...\n",
    "- Each individual test should have a descriptive name\n",
    "    - When a test fails, the first thing you read is the name\n",
    "        - Should describe what was tested and failed\n",
    "    - Should write a docstring to further explain the test\n",
    "    \n",
    "## Placement of test files\n",
    "\n",
    "- Two alternatives, no definite \"best\" solution\n",
    "- See course repository `examples`\n",
    "- Both variants can be run in the same way from PyCharm by adding a suitable PyTest configuration\n",
    "- **We will use variant 1**\n",
    "\n",
    "### Variant 1: tests parallel to code directory\n",
    "\n",
    "- Based on recommendations by the [Python Packaging Project](https://packaging.python.org/en/latest/tutorials/packaging-projects/)\n",
    "\n",
    "```\n",
    "chutes_project/\n",
    "   src/\n",
    "       chutes/\n",
    "          __init__.py\n",
    "          board.py\n",
    "          ...\n",
    "   examples/\n",
    "   tests/\n",
    "      test_board.py\n",
    "   setup.py\n",
    "```\n",
    "- `tests` is a directory \"parallel\" to `chutes` code directory\n",
    "- `tests` is *not* a package\n",
    "- Test files use absolute imports\n",
    "```python\n",
    "from chutes.board import Board\n",
    "```\n",
    "- PyTest configuration in PyCharm should cover `tests` directory\n",
    "\n",
    "\n",
    "### Variant 2: tests in code directory\n",
    "```\n",
    "chutes_project_alt/\n",
    "    src/\n",
    "        chutes/\n",
    "          __init__.py\n",
    "          board.py\n",
    "          ...\n",
    "          tests/\n",
    "             __init__.py\n",
    "             test_board.py\n",
    "   examples/\n",
    "   setup.py\n",
    "```\n",
    "- `tests` is subdirectory of `chutes` code directory\n",
    "- `tests` is a package (contains `__init__.py`)\n",
    "- Test files use relative imports\n",
    "```python\n",
    "from ..board import Board\n",
    "```\n",
    "- PyTest configuration in PyCharm should cover `chutes/tests` directory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "\n",
    "## Suggestions for test design\n",
    "\n",
    "- Test code should be simple: if you cannot understand a test, it is not worth much\n",
    "- Have only a single `assert` in each test: the test fails on the first failing assert, all checks in later asserts will not be performed\n",
    "- If you use \"magic values\", document how you obtained them or best, compute them explicitly (but do not copy-paste code!)\n",
    "- Use variables for input values instead of literal numbers—improved reliability\n",
    "\n",
    "### Poor example\n",
    "\n",
    "```python\n",
    "def test_growing():\n",
    "    a = Baby()\n",
    "    for _ in range(10):\n",
    "        a.grow()\n",
    "    assert a.age == 10\n",
    "    assert a.height == 55\n",
    "```\n",
    "\n",
    "### Good example\n",
    "\n",
    "```python\n",
    "def test_age_increase():\n",
    "    num_days = 10\n",
    "    baby = Baby()\n",
    "    for _ in range(num_days):\n",
    "        baby.grow()\n",
    "    assert baby.age == num_days\n",
    "    \n",
    "def test_height_increase():\n",
    "    num_days = 10\n",
    "    baby = Baby()\n",
    "    for _ in range(num_days):\n",
    "        baby.grow()\n",
    "    assert baby.height == baby.birth_height + num_days * baby.growth_rate\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Approximate comparisons"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pytest import approx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check if two numbers are equal to within a relative error of $10^{-6}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "3.001 == approx(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "3.0000001 == approx(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparing to zero uses absolute error of $10^{-12}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "0.0001 == approx(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "0.0000000000001 == approx(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Approximate comparisons also work for composite data types:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "[1.000001, 3] == approx([1.000001, 3]) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "{'a': 1.000001, 'b': 3} == approx({'a': 1.000001, 'b': 3}) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.array([1.000001, 3]) == approx(np.array([1.000001, 3]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See https://docs.pytest.org/en/latest/reference.html#pytest-approx for details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Test parameterization\n",
    "\n",
    "- Parameterize tests: run one test several times with different values\n",
    "- For more information, see http://pytest.readthedocs.io/en/latest/parametrize.html#parametrize\n",
    "    \n",
    "### Poor example\n",
    "\n",
    "```python\n",
    "def test_default_board_adjustments():\n",
    "    \"\"\"Some tests on default board.\"\"\"\n",
    "\n",
    "    brd = Board()\n",
    "    assert brd.position_adjustment(1) == 39\n",
    "    assert brd.position_adjustment(2) == 0\n",
    "    assert brd.position_adjustment(33) == -30\n",
    "```\n",
    "\n",
    "### Better solution with parameterization\n",
    "\n",
    "```python\n",
    "@pytest.mark.parametrize(\"from_pos, to_pos\",\n",
    "                         [[1, 40],\n",
    "                          [2, 2],\n",
    "                          [33, 3]])\n",
    "def test_default_board_adjustments(from_pos, to_pos):\n",
    "    \"\"\"Test chutes and ladders on default board.\"\"\"\n",
    "\n",
    "    brd = Board()\n",
    "    assert from_pos + brd.position_adjustment(from_pos) == to_pos\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
