#! /usr/bin/env python

"""
Script to run Chutes for profiling.
"""

from chutes.simulation import Simulation
from chutes.board import Board
from chutes.player import *


__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def demo():

    board = Board()
    sim = Simulation([Player(board),
                      LazyPlayer(board, dropped_steps=5),
                      ResilientPlayer(board, extra_steps=5)],
                     board=board)
    sim.run_simulation(100000)


if __name__ == "__main__":
    demo()
